import React from "react";

function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div>
      <header></header>
      <main>{children}</main>
    </div>
  );
}

export default Layout;
