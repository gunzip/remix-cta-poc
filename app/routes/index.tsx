import type { MetaFunction } from "remix";
import Layout from "~/components/layout";

export let meta: MetaFunction = () => {
  return {
    title: "INPS | APE Sociale",
  };
};

export default function Index() {
  return <Layout>hello</Layout>;
}
